import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import {HomeComponent} from './home/home.component'
const routes: Routes = [
  {
    path: 'Internautes/connexion',
    component: ConnexionComponent
  },                                                                                    
  {
		path: 'Internautes/inscription',
		component: InscriptionComponent
	},
  {
    path:'',
    component: HomeComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
