import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { //on met ici la liste 
  title = 'Demo';//*ngFor c'est des directives structurelles c'est une boucle
  //<ul *ngFor="let produit .............">

}
//observable c'est une promesse on créer un service qui va au travers service angular le service mongo qui va permettre 
//de récuperer des données on fait l'injection de dépendace du service dans cette classe avec :
// constructor (private produitsService .......)
/* Any[] pour dire que c'est une collection de n'importe quoi 
le service ne ce met pas à jour tout seul il faut le faire manuelllement avec :
import { HttpClientModule} from ....... dans app.module.ts
import {ProduitService} ............
c'est la première config a faire
puis dans produits.service.ts on ajoute dans les imports HttpClient et dans providers le service créer

=> Dès qu'on crée un service en faisant ng g s <nomservice> il faut configurer les fichiers "app.module.ts" ou on fait 
2 config : dans import et providers ou on ajoute le faite qu'on a créer un service
le faire aussi pour HttpClient dans imports du fichier dans les objets ajouter au décorateur

*/