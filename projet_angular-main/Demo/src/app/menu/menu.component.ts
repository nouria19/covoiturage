import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
//app-menu c'est une component directive 
//une directive c'est une extension d'une balise html
//router-outlet c'est une zone dynamique
//c'est toujours le dernier template qui a été invoqué dans le router-outlet

export class MenuComponent implements OnInit {
  public user: Observable<string>;

  constructor(private authService: AuthentificationService,
              private router: Router) {
        this.user = this.authService.getUser(); }

  ngOnInit() {
  }

  deconnexion() {
        this.authService.disconnect();
        this.router.navigate(['/']);
  }
}
