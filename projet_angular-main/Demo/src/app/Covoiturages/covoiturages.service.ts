import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CovoiturageService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  rechercherCovoiturage(departureCity: string, arrivalCity: string, date: string) {
    const url = `${this.baseUrl}/Covoiturages/recherche?villeDepart=${departureCity}&villeArrivee=${arrivalCity}&date=${date}`;
    return this.http.get(url);
  }
}
