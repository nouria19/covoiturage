import { TestBed } from '@angular/core/testing';
import { CovoiturageService } from './covoiturages.service';



describe('CovoituragesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CovoiturageService = TestBed.get(CovoiturageService);
    expect(service).toBeTruthy();
  });
});