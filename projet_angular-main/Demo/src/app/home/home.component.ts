import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CovoiturageService } from '../Covoiturages/covoiturages.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchForm: FormGroup = new FormGroup({});
villeArrivee: string='';
villeDepart: string='';
dateDepart:string='';
searchResults: any;


  constructor(private formBuilder: FormBuilder, private router: Router,private covoiturageService: CovoiturageService) {
    
   }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      departureCity: ['', Validators.required],
      arrivalCity: ['', Validators.required],
      date: ['', Validators.required],
      price: ['']
    });
  }

  onSubmit() {
    // Get form values
    const departureCity = this.searchForm.value.departureCity;
    const arrivalCity = this.searchForm.value.arrivalCity;
    const date = this.searchForm.value.date;
    

    // Call the search service
    this.covoiturageService.rechercherCovoiturage(departureCity, arrivalCity, date)
      .subscribe((results: any) => {
        console.log(results); 
      });
  }
}