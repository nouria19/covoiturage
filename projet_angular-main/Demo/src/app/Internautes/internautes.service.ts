import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InternautesService {

  private urlBase: string = 'http://localhost:8080/';

    constructor(private http: HttpClient) { }

    getInternautes(): Observable<any> {
        return this.http.get(this.urlBase+'Internautes');//pour recuperre la valeur d'un observable on utilise methode subsribe 
    }

    getCovoiturages(): Observable<any> {
        return this.http.get(this.urlBase+'Covoiturages');//cette ligne demande les route au serveur node
    }
}
