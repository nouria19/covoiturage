import { TestBed } from '@angular/core/testing';

import { InternautesService } from './internautes.service';

describe('InternautesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternautesService = TestBed.get(InternautesService);
    expect(service).toBeTruthy();
  });
});