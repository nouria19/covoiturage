var express = require("express");
var app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const saltRounds = 10; // le nombre de rounds de salage

// Middleware pour parser les requêtes
app.use(express.urlencoded({ extended: true }));//ng generate pour ajouter des composants dans projet angular
app.use(express.json())

// Middleware pour éviter les problèmes Cross
app.use(function (req, res, next) {//avec use demande au serveur que toutes les requetes renvoyer aient un head modifier
    res.setHeader('Access-Control-Allow-Origin', '*');//pour que les requêtes fonctionne pour n'importe qui sinon on va avoir problème cross
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');//pour éviter un probblème de sécurité cross
    res.setHeader('Access-Control-Allow-Headers', '*');
    next();
});

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://127.0.0.1:27017";
app.listen(8080, () => {
    console.log("Serveur Express en écoute sur le port 8080");
});

//service web ecouteur de requete ce fait avec express
// Connexion à la base de données et définition des routes
const client = new MongoClient(url,{ useNewUrlParser: true, useUnifiedTopology: true });

// Hasher les mots de passe des utilisateurs existants
async function hashPasswords() {
  try {
    await client.connect();

    const db = client.db('SUPERVENTES');
    const collection = db.collection('Internautes');

    // Trouver tous les documents dans la collection "Internautes"
    const users = await collection.find().toArray();

    // Hasher le mot de passe de chaque utilisateur
    for (const user of users) {
      const hashedPassword = await bcrypt.hash(user.motDePasse, 10);
      user.motDePasse = hashedPassword;
      await collection.updateOne({ _id: user._id }, { $set: { motDePasse: hashedPassword } });
    }

    console.log('Tous les mots de passe ont été hashés avec succès.');
  } catch (err) {
    console.error(err);
  } finally {
    await client.close();z
  }
}
app.get('/Covoiturages/recherche', async (req, res) => {
  const villeDepart = req.query.villeDepart;
  const villeArrivee = req.query.villeArrivee;
  const date = req.query.date;
  const prixMax = req.query.prixMax;

  // Rechercher le prix moyen des trajets entre les deux villes
  const documents = await db.collection('Covoiturages').aggregate([
    { $match: { villeDepart: villeDepart, villeArrivee: villeArrivee } },
    { $group: { _id: null, avgPrice: { $avg: '$prix' } } }
  ]).toArray();
  const prixMoyen = documents.length > 0 ? documents[0].avgPrice : 0;

  // Si aucun prix maximum n'est spécifié, utilisez le prix moyen des trajets entre les deux villes
  const prixMaximal = prixMax ? prixMax : prixMoyen;

  // Rechercher les covoiturages correspondant aux critères de recherche
  const covoiturages = await db.collection('Covoiturages').find({
    villeDepart: villeDepart,
    villeArrivee: villeArrivee,
    date: date,
    prix: { $lte: prixMaximal }
  }).toArray();

  res.json(covoiturages);
});
//  assure que seuls les utilisateurs authentifiés peuvent accéder aux ressources protégées c'est un  middleware 
function requireLogin(req, res, next) {
  const token = req.headers.authorization.split(' ')[1];
  console.log('Token reçu :', token);

  if (!token) {
    return res.status(401).json({ message: 'Authentification nécessaire.' });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded;
    next();
  } catch (err) {
    return res.status(401).json({ message: 'Token invalide.' });
  }
}


async function main() {
	client.connect()
    .then(
        client => {
			db = client.db("SUPERVENTES");//variable qui sera connu dans tout le service web
      // Appeler la fonction pour hasher les mots de passe
      //hashPasswords();
		// Route pour récupérer tous les covoiturages
        app.get("/Covoiturages", async (req, res) => {
            console.log("/Covoiturages");
            const documents = await db.collection("Covoiturages").find().toArray();
            res.json(documents);
        });

        // Route pour récupérer tous les Internautes
        app.get("/Internautes", async (req, res) => {
            console.log("/Internautes");
            const documents = await db.collection("Internautes").find().toArray();
            res.json(documents);
        });
       //méthode d'inscription
app.post('/Internautes/inscription', async (req, res) => {
  console.log('/Internautes/inscription avec ', req.body);

  // Vérifiez si l'adresse e-mail existe déjà dans la base de données
  const emailExists = await db.collection('Internautes').findOne({ email: req.body.email });
  if (emailExists) {
    res.json({ resultat: 0, message: 'L\'adresse e-mail est déjà enregistrée.' });
    return;
  }

  // Hachez le mot de passe avant de l'ajouter à la base de données
  const hashedPassword = await bcrypt.hash(req.body.motDePasse, saltRounds);

  // Créez un nouvel utilisateur avec les informations fournies par l'utilisateur, y compris le mot de passe haché
  const newInternaute = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    email: req.body.email,
    motDePasse: hashedPassword,
    téléphone: req.body.téléphone
  };

  // Insérez le nouvel utilisateur dans la base de données
  await db.collection('Internautes').insertOne(newInternaute);

  // Renvoyer une réponse de réussite
  res.json({ resultat: 1, message: 'Inscription réussie.' });
});


        // Méthode de connexion avec les tokens pour améliorer la sécurité


        app.post('/Internautes/connexion', async (req, res) => {
          console.log('/Internautes/connexion avec ', req.body);
        
          // Vérifiez si l'utilisateur existe dans la base de données
          const user = await db.collection('Internautes').findOne({ email: req.body.email });
          if (!user) {
            res.json({ resultat: 0, message: 'Email et/ou mot de passe incorrect' });
            return;
          }
        
          // Vérifiez si le mot de passe est correct
          const passwordMatch = await bcrypt.compare(req.body.motDePasse, user.motDePasse);
          if (!passwordMatch) {
            res.json({ resultat: 0, message: 'Email et/ou mot de passe incorrect' });
            return;
          }
        
          // Générez un token d'authentification pour l'utilisateur
          const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET,{ expiresIn: '1h' });
          console.log('code secret dans sign :',process.env.JWT_SECRET);
          // Renvoyer une réponse de réussite avec le token
          res.json({ resultat: 1, message: 'Authentification réussie', token });
        });
        
/* =========================================== les méthodes crud pour Covoiturages ========================================= */


// methode pour la sélectionne d'un covoiturage

    app.put("/Covoiturages/selectionner/:id_covoiturage", async (req, res) => {
  console.log("/Covoiturages/selectionner avec ", req.body);
  const id_covoiturage = req.params.id_covoiturage;
  const email_internaute = req.body.email;

  try {
    const covoiturage = await db.collection("Covoiturages").findOne({ _id: ObjectId(id_covoiturage) });
    if (!covoiturage) {
      res.status(404).json({ resultat: 0, message: "Le covoiturage n'existe pas" });
      return;
    } else if (covoiturage.email === email_internaute) {
      res.status(400).json({ resultat: 0, message: "Vous ne pouvez pas vous inscrire à votre propre covoiturage" });
      return;
    } else if (covoiturage.nbPlaces === 0) {
      res.status(400).json({ resultat: 0, message: "Il n'y a plus de places disponibles dans ce covoiturage" });
      return;
    }

    const transport = {
      idCovoiturage: ObjectId(id_covoiturage),
      email: email_internaute,
    };
    const result = await db.collection("Transports").insertOne(transport);

    await db.collection("Covoiturages").updateOne(
      { _id: ObjectId(id_covoiturage) },
      { $inc: { nbPlaces: -1 } }
    );

    res.json({ resultat: 1, message: "Le covoiturage a été sélectionné avec succès" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ resultat: 0, message: "Une erreur est survenue lors de la sélection du covoiturage." });
  }
});


    app.put("/Covoiturages/modifier/:email", async (req, res) => {
        console.log("/Covoiturages/modifier avec ", req.body);
        const email = req.params.email;
        const update = req.body;

        try {
            const result = await db.collection("Covoiturages").updateOne({ email: email }, { $set: update });
            if (result.matchedCount === 0) {
              res.status(404).json({ resultat: 0, message: "Aucun utilisateur trouvé avec cet Email." });
              return;
            }
            res.json({ resultat: 1, message: "Mise à jour réussie." });
          } catch (error) {
            console.error(error);
            res.status(500).json({ resultat: 0, message: "Une erreur est survenue lors de la mise à jour." });
          }

    });

    app.put("/Internautes/:id",requireLogin, async (req, res) => {
        const id = req.params.id;
        const update = req.body;
      
        try {
          const result = await db.collection("Internautes").updateOne({ _id: ObjectId(id) }, { $set: update });
          if (result.matchedCount === 0) {
            res.status(404).json({ resultat: 0, message: "Aucun utilisateur trouvé avec cet ID." });
            return;
          }
          res.json({ resultat: 1, message: "Mise à jour réussie." });
        } catch (error) {
          console.error(error);
          res.status(500).json({ resultat: 0, message: "Une erreur est survenue lors de la mise à jour." });
        }
      });
    
        // Route pour récupérer tous les Transports
        app.get("/Transports",requireLogin, async (req, res) => {
            console.log("/Transports");
            const documents = await db.collection("Transports").find().toArray();
            res.json(documents);
        });


        //Le middleware "requireLogin" est utilisé pour s'assurer que seuls les utilisateurs authentifiés peuvent accéder aux ressources protégées.
  

        //Proposer un covoiturage par un internautes qui a les informations permis et immatriculations
        app.post('/api/covoiturages', requireLogin, async (req, res) => {
          const { villeDepart, villeArrivee, date, nbPlaces } = req.body;
        
          // Vérifier que les informations sont valides
          if (!villeDepart || !villeArrivee || !date || !nbPlaces) {
            return res.status(400).json({ message: 'Informations manquantes' });
          }
        
          // Récupérer les informations du conducteur depuis son profil
          const { numPermis, numImmatriculation } = await Internautes.findById(req.session.userId);
        
          // Vérifier que le conducteur a bien les informations nécessaires
          if (!numPermis || !numImmatriculation) {
            return res.status(400).json({ message: 'Le conducteur doit fournir un numéro de permis et le numéro d\'immatriculation de son véhicule' });
          }
        
          // Vérifier que la date est au bon format
          const dateRegex = /^\d{6}$/;
          if (!dateRegex.test(date)) {
            return res.status(400).json({ message: 'La date doit être au format AAMMJJ' });
          }
        
          // Créer le nouveau covoiturage
          const newCovoiturage = new Covoiturages({
            villeDepart,
            villeArrivee,
            date,
            conducteur: req.session.userId,
            nbPlaces,
            prix: 0 // TODO: calculer le prix en fonction du prix moyen des trajets entre les deux villes
          });
        
          try {
            await newCovoiturage.save();
            res.status(201).json(newCovoiturage);
          } catch (error) {
            res.status(500).json({ message: 'Erreur serveur lors de la création du covoiturage' });
          }
        });


     //async c'est des requete asynchrone
		/* Route pour récupérer les covoiturages selon la ville de départ et d'arrivée, */
        app.get("/Covoiturages/:villeDepart/:villeArrivee",requireLogin, async (req, res) => {
          const { villeDepart, villeArrivee } = req.params;
          console.log(`/Covoiturages/${villeDepart}/${villeArrivee}`);
          const documents = await db
              .collection("Covoiturages")
              .find({ villeDepart, villeArrivee })
              .toArray();
          res.json(documents);
      });


      app.get('/Covoiturages/recherche1',requireLogin, async (req, res) => {
  const villeDepart = req.query.villeDepart;
  const villeArrivee = req.query.villeArrivee;
  const date = req.query.date;
  const prixMax = req.query.prixMax;

  // Rechercher le prix moyen des trajets entre les deux villes
  const documents = await db.collection('Covoiturages').aggregate([
    { $match: { villeDepart: villeDepart, villeArrivee: villeArrivee } },
    { $group: { _id: null, avgPrice: { $avg: '$prix' } } }
  ]).toArray();
  const prixMoyen = documents.length > 0 ? documents[0].avgPrice : 0;

  // Si aucun prix maximum n'est spécifié, utilisez le prix moyen des trajets entre les deux villes
  const prixMaximal = prixMax ? prixMax : prixMoyen;

  // Rechercher les covoiturages correspondant aux critères de recherche
  const covoiturages = await db.collection('Covoiturages').find({
    villeDepart: villeDepart,
    villeArrivee: villeArrivee,
    date: date,
    prix: { $lte: prixMaximal }
  }).toArray();

  res.json(covoiturages);
});

//il faut selectionner toutes les données du covoiturage 
//(ville de départ, ville d'arrivée, date, email du conducteur et nombre de places disponibles)
app.post('/Transports/selection', requireLogin, async (req, res) => {
  console.log('/Transports/selection avec ', req.body);

  // Vérifiez si le covoiturage existe dans la base de données
  const covoiturage = await db.collection('Covoiturages').findOne({
    villeDépart: req.body.villeDépart,
    villeArrivée: req.body.villeArrivée,
    date: req.body.date,
    email: req.body.email,
    nbPlaces: { $gt: 0 }
  });

  if (!covoiturage) {
    res.json({ resultat: 0, message: 'Aucun covoiturage correspondant n\'a été trouvé.' });
    return;
  }

  // Empêchez le conducteur de sélectionner son propre covoiturage
  if (covoiturage.email === req.user.email) {
    res.json({ resultat: 0, message: 'Vous ne pouvez pas sélectionner votre propre covoiturage.' });
    return;
  }

  // Décrémentez le nombre de places disponibles dans le covoiturage
  const updateResult = await db.collection('Covoiturages').updateOne(
    { _id: covoiturage._id },
    { $inc: { nbPlaces: -1 } }
  );

  if (updateResult.modifiedCount === 0) {
    res.json({ resultat: 0, message: 'Impossible de réserver une place dans ce covoiturage.' });
    return;
  }

  // Créez un nouveau document dans la collection Transports
  const transport = {
    idCovoiturage: covoiturage._id,
    email: req.user.email
  };

  const insertionResult = await db.collection('Transports').insertOne(transport);

  if (insertionResult.insertedCount !== 1) {
    res.json({ resultat: 0, message: 'Erreur lors de la création du document de transport.' });
    return;
  }

  res.json({ resultat: 1, message: 'Transport réservé avec succès.' });
});	
	});
}
main();



